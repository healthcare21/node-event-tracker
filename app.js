const express = require("express");
const app = express();
const port = 8080;
const fs = require("fs-extra");
var bodyParser = require("body-parser");

var http = require("http");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
let timestamp;
app.get("/", (req, res) => res.send("Hello World!"));
app.get("/addnewevent", function(req, res) {
  //   console.log(req);
  var date = new Date();
  var currentDate = getDate();
  timestamp = date.valueOf();
  //setr item in the database
  const store = require("data-store")({
    path: process.cwd() + "/analytics/" + timestamp + ".json"
  });
  store.set({ name: timestamp, data: { Date: currentDate } });

  res.send("Added a new analytics event");
});
app.post("/postevent", function(req, res) {
  var newData = req.body;
  const store = require("data-store")({
    path: process.cwd() + "/analytics/" + timestamp + ".json"
  });

  var storedata = store.data;

  // console.log(newData.storedata.data);
  let objKeys = Object.keys(newData.storedata.data);
  for (var i = 0; i < objKeys.length; i++) {
    storedata.data[objKeys[i]] = newData.storedata.data[objKeys[i]];
  }

  store.set(storedata);
  // var date = new Date();
  // var currentDate = getDate();
  // var timestamp = date.valueOf();
  // //setr item in the database
  // const store = require("data-store")({
  //   path: process.cwd() + "/analytics/" + timestamp + ".json"
  // });
  // store.set({ name: timestamp, data: { Date: currentDate } });
  res.send("Added a new analytics event");
});

function getDate() {
  var date = new Date();
  var currentDate =
    "Day:" +
    date.getDate() +
    " Month: " +
    date.getMonth() +
    " Time: " +
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds();
  return currentDate;
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
